package com.service.surbhiservice.feignclients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.service.surbhiservice.model.FinancialReportDetails;
import com.service.surbhiservice.model.UserDetails;

@FeignClient(url = "http://localhost:8081/admin", name = "Admin-Client")
public interface AdminClient {

	@GetMapping("/users/add")
	public String addUser(@RequestParam("user") UserDetails user);

	@GetMapping("/users/update")
	public String updateUser(@RequestParam("user") UserDetails user);

	@GetMapping("/users/fetch")
	public UserDetails fetchUser(@RequestParam("id") int id);

	@GetMapping("/users/delete")
	public String deleteUser(@RequestParam("id") int id);

	@GetMapping("/viewDailyBillings")
	public List<FinancialReportDetails> getDailyReport();

	@GetMapping("/viewMonthlyBillings")
	public List<FinancialReportDetails> getMonthlyReport();

}
