package com.service.surbhiservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.surbhiservice.feignclients.FestiveOfferClient;

@RestController
@RequestMapping("/surabi/festiveOffer")
public class FestiveOfferController {

	@Autowired
	FestiveOfferClient festiveOfferClient;

	@PostMapping("/applyOfferFromSurabi")
	public String applyOffer(String couponCode) {
		return festiveOfferClient.applyOffer(couponCode);
	}

}
