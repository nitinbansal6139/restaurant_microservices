package com.service.surbhiservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.surbhiservice.feignclients.UserClient;
import com.service.surbhiservice.model.FinancialReportDetails;
import com.service.surbhiservice.model.ItemDetails;
import com.service.surbhiservice.model.SelectItem;

@RestController("/surabi/users")
public class UserController {

	@Autowired
	UserClient userClient;

	@GetMapping("/viewItemsFromSurabi")
	public List<ItemDetails> viewItems() {
		return userClient.viewItems();
	}

	@PostMapping("/selectItemsbyIdsFromSurabi")
	public String selectItemDetailsByIds(@RequestBody List<SelectItem> selectItem) {
		return userClient.selectItemByIds(selectItem);
	}

	@GetMapping("/viewFinalBillFromSurabi")
	public List<FinancialReportDetails> getFinalBill() {
		return userClient.getFinalBill();
	}

}
