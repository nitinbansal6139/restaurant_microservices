package com.service.userservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.userservice.entity.FinancialReport;

@Repository
public interface FinancialReportRepository extends JpaRepository<FinancialReport, Integer> {

	@Query(value = "from FinancialReport t where t.purchaseDate >= CURDATE()")
	List<FinancialReport> findTodaysPurchases();
}
