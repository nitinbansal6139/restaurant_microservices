package com.service.userservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.userservice.entity.FinancialReport;
import com.service.userservice.entity.Item;
import com.service.userservice.model.SelectItem;
import com.service.userservice.service.RestaurantItemService;


@RestController
@RequestMapping("/restaurant")
public class RestaurantItemController {

	@Autowired
	RestaurantItemService restaurantItemService;

	@GetMapping("/viewItems")
	public List<Item> viewItems() {
		return restaurantItemService.viewItemDetails();
	}

	@PostMapping("/selectItemsbyIds")
	public String selectItemDetailsByIds(@RequestBody List<SelectItem> selectItem) {
		return restaurantItemService.selectItemDetailsByIds(selectItem);
	}

	@GetMapping("/viewFinalBill")
	public List<FinancialReport> getFinalBill() {
		return restaurantItemService.viewFinalBill();
	}

}
