package com.service.userservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.service.userservice.entity.FinancialReport;
import com.service.userservice.entity.Item;
import com.service.userservice.model.SelectItem;

@Service
public interface RestaurantItemService {

	List<Item> viewItemDetails();
	
	String selectItemDetailsByIds(List<SelectItem> selectItem);
	
	List<FinancialReport> viewFinalBill();
	
}
