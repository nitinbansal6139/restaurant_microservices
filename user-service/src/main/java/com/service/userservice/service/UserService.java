package com.service.userservice.service;

import org.springframework.stereotype.Service;

import com.service.userservice.entity.User;

@Service
public interface UserService {

	String createUser(User user);

}
