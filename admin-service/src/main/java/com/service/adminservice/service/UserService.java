package com.service.adminservice.service;

import org.springframework.stereotype.Service;

import com.service.adminservice.entity.User;

@Service
public interface UserService {

	String createUser(User user);
	
	String updateUser(User user);
	
	String deleteUser(int id);
	
	User fetchUser(int id);
}
