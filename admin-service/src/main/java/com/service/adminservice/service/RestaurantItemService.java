package com.service.adminservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.service.adminservice.entity.FinancialReport;

@Service
public interface RestaurantItemService {

	List<FinancialReport> viewDailyReport();
	
	List<FinancialReport> totalMonthlySale();
}
