package com.service.adminservice.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.service.adminservice.entity.User;
import com.service.adminservice.repository.UserRepository;
import com.service.adminservice.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepo;
	@Override
	public String createUser(User user) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		userRepo.save(user);
		return "User is created with role "+user.getRole();
	}

	@Override
	public String updateUser(User user) {
		User userFound = userRepo.findByUsername(user.getUserName());
		if(userFound == null) {
			return "User not found with username "+user.getUserName();
		}
		user.setId(userFound.getId());
		userRepo.save(user);
		return "User updated";
	}

	@Override
	public String deleteUser(int id) {
		Optional<User> userFound = userRepo.findById(Long.valueOf(id));
		if(!userFound.isPresent()) {
			return null;
		}
		userRepo.deleteById(Long.valueOf(id));
		return "User deleted";
	}

	@Override
	public User fetchUser(int id) {
		Optional<User> userFound = userRepo.findById(Long.valueOf(id));
		if(!userFound.isPresent()) {
			return null;
		}
		return userFound.get();
	}

}
