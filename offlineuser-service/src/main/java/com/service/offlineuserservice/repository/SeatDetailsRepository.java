package com.service.offlineuserservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.offlineuserservice.entity.Seat;

@Repository
public interface SeatDetailsRepository extends JpaRepository<Seat,Integer>{

}
