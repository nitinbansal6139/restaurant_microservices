package com.service.offlineuserservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.offlineuserservice.entity.FinancialReport;
import com.service.offlineuserservice.entity.Item;
import com.service.offlineuserservice.model.SelectItem;
import com.service.offlineuserservice.service.RestaurantItemService;
import com.service.offlineuserservice.service.SeatBookingService;

@RestController
@RequestMapping("/offlineUserServices")
public class OfflineUserController {
	
	@Autowired
	RestaurantItemService itemService;
	
	@Autowired
	SeatBookingService seatBooking;
	
	@GetMapping("/viewItems")
	public List<Item> getItems(){
		return itemService.viewItemDetails();
	}
	
	@PostMapping("/selectItemsbyIds")
	public String selectItemDetailsByIds(@RequestBody List<SelectItem> selectItem) {
		return itemService.selectItemDetailsByIds(selectItem);
	}

	@GetMapping("/viewFinalBill")
	public List<FinancialReport> getFinalBill() {
		return itemService.viewFinalBill();
	}
	
	@PostMapping("/payBill")
	public String payBill(int amount, String paymentMethod) {
		return itemService.payBill(amount, paymentMethod);
	}
	
	@PostMapping("/postReview")
	public String postReview(int rating, String comments) {
		return itemService.giveReview(rating, comments);
	}
}
